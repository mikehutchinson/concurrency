# Concurrency Service

## Brief

The brief is to develop a node microservice to track the number of concurrent streams a user has, and to error when this is more than 3.

## Assumptions

* This service will live behind a load balancer configured to implement sticky sessions based on a (public) user identifier included in the JWT Access Tokens.
* A separate upstream service/module will have already verified the user's session and entitlement to stream the content
* Stream heartbeats are expected to occur every 30s, however a client can miss _one_ heartbeat.  Therefore if a stream is _not_ explicitly stopped, it will still be seen as active up to 59.999s following the last successfully recorded heartbeat.
* The user's JWT is assumed to have been validated by an upstream service.

## Design considerations

Typical DRM concurrency solutions utilise a heartbeating mechanism for maintaining streams (e.g. Widevine and Playready), therefore this service will follow the same pattern.  As this is not managing resources, the HTTP API will _not_ be RESTful.

## API

* POST `/start/{contentId}` - Start the stream for the content with the given ID
  * Returns `HTTP 200` with a GUID for the stream which will be used in subsequent heartbeats
  * Returns `HTTP 403` when number of concurrent streams is exceeded

* POST `/heartbeat/{streamId}` - Heartbeat to track playback still occurring
  * Returns `HTTP 204` (regardless of whether the given stream ID exists)

* POST `/stop/{streamId}` - Stop the stream with the given ID
  * Returns `HTTP 204` (regardless of whether the given stream ID exists)

## Technology choices

* Node 10 LTS
* Jest
* Express
* Data persistence mechanism is a simple in memory Map of Maps.

## Building & Testing & Documentation

Install the dependencies:

```sh
npm i
```

Run the linting:

```sh
npm run lint
```

Run the tests:

```sh
npm run test
```

Coverage reports will be written to `./coverage` and are also published to [Gitlab Pages](https://mikehutchinson.gitlab.io/concurrency/coverage/index.html)

Build the docs:

```sh
npm run doc
```

JSDocs will be written to `./docs` and are also published to [Gitlab Pages](https://mikehutchinson.gitlab.io/concurrency/index.html)

Additionally, Swagger API docs are generated and hosted at [/api-docs](http://localhost:3000/api-docs) when the service is running.

## Running

```sh
  npm run start
```

The service will then be available on [http://localhost:3000](http://localhost:3000).  See the [Swagger API documentation](http://localhost:3000/api-docs) for more information on the available endpoints.

## Potential future enhancements

* Heartbeating coult/should be extended to ensure the provided `streamId` is valid, and error if so.
* The stream store could be replaced with various potential data stores, all that would be required is an adapter meeting the `streams` api.
* Extract the concurrency limit, expiry and JWT secret into either config or provided from an external store such as AWS ParameterStore.
* Reporting of stream metrics could be added resonably simply.
* Timed cleanup of expired streams and empty `userStreams` maps, the current implementation assumes that `endStream` will always be called.
* Use a better swagger generator.
* A separate integration test suite and postman collection
