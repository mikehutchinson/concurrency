/** @module streams */
const uuid = require('uuid/v4');

// Initial store, may be replaced with a dedicated module later.
// Will be a Map of Maps. Map[userID, Map[streamId, StreamInfo]]
let streams;

/**
 * Resets the streams store.
 */
const resetStreams = () => {
  streams = new Map();
};

/**
 * Get's the streams map for the given userId.
 * If the map does not exist, it will be created.
 * @param {String} userId
 *
 * @returns {Map} Map of this user's streams to be used in subsequent operations
 */
const getUserStreams = (userId) => {
  if (!streams.has(userId)) {
    streams.set(userId, new Map());
  }
  return streams.get(userId);
};

/**
 * Get a single stream from the provided `userStreams` using the provided `streamId`.
 *
 * @param {Map} userStreams The user's streams, retrieved from #getUserStreams()
 * @param {String} streamId The ID of the stream being requested.
 *
 * @returns {Object?} stream or null if no stream found.
 */
const getUserStream = (userStreams, streamId) => {
  if (!userStreams || !userStreams.has(streamId)) {
    return null;
  }

  return { ...userStreams.get(streamId) };
};

/**
 * Add a stream to the provided `userStreams` for the provided `contentId`.

 * @param {Map} userStreams The user's streams, retrieved from #getUserStreams()
 * @param {String} contentId The ID of the content to record
 *
 * @returns {String?} Unique identifier for this stream,
 *                    or null if the provided userStreams is invalid.
 */
const addUserStream = (userStreams, contentId) => {
  if (!userStreams) {
    return null;
  }

  // Generate a unique ID for this stream.
  const streamId = uuid();
  const now = Date.now();
  userStreams.set(streamId, {
    id: streamId,
    contentId,
    createdAt: now,
    updatedAt: now,
  });
  return streamId;
};

/**
 * Remove the stream with the given `streamId` from the provided `userStreams`.
 * No-op if provided `userStreams` is invalid or stream does not exist.
 *
 * @param {Map} userStreams The user's streams, retrieved from #getUserStreams()
 * @param {String} streamId The ID of the stream being removed.
 */
const removeUserStream = (userStreams, streamId) => {
  if (!userStreams || !userStreams.has(streamId)) {
    return;
  }

  userStreams.delete(streamId);
};

/**
 * Update the `updatedAt` field of the stream with the given `streamId`
 * within the provided `userStreams`.
 * No-op if provided `userStreams` is invalid or stream does not exist.
 *
 * @param {Map} userStreams The user's streams, retrieved from #getUserStreams()
 * @param {String} streamId The ID of the stream being updated.
 */
const updateUserStream = (userStreams, streamId) => {
  const stream = getUserStream(userStreams, streamId);
  if (!stream) {
    return;
  }

  stream.updatedAt = Date.now();
  userStreams.set(streamId, stream);
};

module.exports = {
  getUserStreams,
  getUserStream,
  addUserStream,
  removeUserStream,
  updateUserStream,
  resetStreams,
};

resetStreams();
