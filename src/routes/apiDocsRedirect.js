// redirect from / to /api-docs
module.exports = (req, res) => res.redirect('/api-docs');
