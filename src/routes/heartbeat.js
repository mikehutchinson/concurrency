/** @module heartbeatRoutes */
const { decodeUserIdToken, getToken } = require('./helper');
const concurrency = require('../concurrency');

/**
 * Perform a heartbeat for the stream with the given `streamId`.
 *
 * @route POST /heartbeat/{streamId}
 * @group Streams - Start and End streams
 * @param {string} streamId.path - ID of stream
 * @returns {void} 204
 * @security JWT
 */
const heartbeat = (req, res) => {
  const userId = decodeUserIdToken(getToken(req));
  const { streamId } = req.params;

  concurrency.refreshStream(userId, streamId);

  res.status(204).end();
};

module.exports = {
  heartbeat,
};
