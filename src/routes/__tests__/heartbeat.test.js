const { encodeUserIdToken } = require('../helper');

// Mocks
const mockConcurrency = {
  refreshStream: jest.fn(),
};
jest.mock('../../concurrency', () => mockConcurrency);

// Module under test
const { heartbeat } = require('../heartbeat');

const testUserId = 'testUserId';
const testToken = encodeUserIdToken(testUserId);
const testStreamId = 'testStreamId';

describe('startStream', () => {
  let req;

  beforeEach(() => {
    req = {
      headers: {
        authorization: `Bearer ${testToken}`,
      },
      params: {},
    };
  });

  test('returns streamId when ok', () => {
    const res = {
      status: jest.fn().mockReturnThis(),
      end: jest.fn().mockReturnThis(),
    };

    req.params.streamId = testStreamId;

    heartbeat(req, res);

    expect(res.status).toHaveBeenCalledWith(204);
    expect(mockConcurrency.refreshStream).toHaveBeenCalledWith(testUserId, testStreamId);
  });
});
