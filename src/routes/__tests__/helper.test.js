const {
  encodeUserId, encodeUserIdToken, decodeUserIdToken, getToken,
} = require('../helper');

describe('encodeUserIdToken & decodeUserIdToken', () => {
  it('encodes a token which can then be decoded', () => {
    const testUserId = 'testUserId';

    const encoded = encodeUserIdToken(testUserId);

    expect(decodeUserIdToken(encoded)).toBe(testUserId);
  });
});

describe('encodeUserIdToken', () => {
  it('encodes a token which can then be decoded', () => {
    const testUserId = 'testUserId';
    const request = {
      params: {
        userId: testUserId,
      },
    };

    const response = {
      json: jest.fn(),
    };

    encodeUserId(request, response);

    expect(response.json).toHaveBeenCalledWith({
      userId: testUserId,
      token: encodeUserIdToken(testUserId),
    });
  });
});

describe('getToken', () => {
  it('gets token from authorization header', () => {
    const req = {
      headers: {
        authorization: 'Bearer myAuthToken',
      },
    };

    expect(getToken(req)).toBe('myAuthToken');
  });
});
