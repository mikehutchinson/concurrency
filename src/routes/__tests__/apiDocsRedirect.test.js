const apiDocsRedirect = require('../apiDocsRedirect');

describe('api-docs redirect', () => {
  test('redirects to /api-docs', () => {
    const mockResponse = {
      redirect: jest.fn(),
    };
    apiDocsRedirect({}, mockResponse);

    expect(mockResponse.redirect).toHaveBeenCalledWith('/api-docs');
  });
});
