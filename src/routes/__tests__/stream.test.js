const { encodeUserIdToken } = require('../helper');

// Mocks
const mockConcurrency = {
  startStream: jest.fn(),
  endStream: jest.fn(),
};
jest.mock('../../concurrency', () => mockConcurrency);

// Module under test
const { startStream, stopStream } = require('../stream');

const testUserId = 'testUserId';
const testToken = encodeUserIdToken(testUserId);
const testContentId = 'testContentId';
const testStreamId = 'testStreamId';

describe('startStream', () => {
  let req;

  beforeEach(() => {
    req = {
      headers: {
        authorization: `Bearer ${testToken}`,
      },
      params: {},
    };
  });

  test('returns streamId when ok', () => {
    mockConcurrency.startStream.mockImplementationOnce(() => testStreamId);
    const res = {
      json: jest.fn(),
    };

    req.params.contentId = testContentId;

    startStream(req, res);

    expect(res.json).toHaveBeenCalledWith({
      streamId: testStreamId,
    });
    expect(mockConcurrency.startStream).toHaveBeenCalledWith(testUserId, testContentId);
  });

  test('returns 403 when stream errors', () => {
    // Suppress console.error, as it's expected
    console.error = jest.fn();

    mockConcurrency.startStream.mockImplementationOnce(() => { throw new Error('Test'); });
    const res = {
      status: jest.fn().mockReturnThis(),
      end: jest.fn().mockReturnThis(),
    };

    req.params.contentId = testContentId;

    startStream(req, res);

    expect(res.status).toHaveBeenCalledWith(403);
    expect(mockConcurrency.startStream).toHaveBeenCalledWith(testUserId, testContentId);
  });
});

describe('stopStream', () => {
  let req;

  beforeEach(() => {
    req = {
      headers: {
        authorization: `Bearer ${testToken}`,
      },
      params: {},
    };
  });

  test('removes the stream', () => {
    const res = {
      status: jest.fn().mockReturnThis(),
      end: jest.fn().mockReturnThis(),
    };

    req.params.streamId = testStreamId;

    stopStream(req, res);

    expect(res.status).toHaveBeenCalledWith(204);

    expect(mockConcurrency.endStream).toHaveBeenCalledWith(testUserId, testStreamId);
  });
});
