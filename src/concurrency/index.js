/** @module concurrency */
const differenceInMilliseconds = require('date-fns/difference_in_milliseconds');
const {
  getUserStreams,
  addUserStream,
  removeUserStream,
  updateUserStream,
} = require('../streams');

// IDEA Make configurable
const expiryThreshold = 59999; // Millis. Allow 1 missed 30s heartbeat.
const numAllowedStreams = 3;

/**
 * Check if the provided stream has expired.
 *
 * @private
 * @param {Object} stream The stream to check.
 * @returns {boolean} true, if stream is expired.
 */
const isStreamExpired = stream => differenceInMilliseconds(Date.now(), stream.updatedAt) > expiryThreshold;

/**
 * Removes expired streams.
 *
 * @private
 * @param {Map} userStreams The user's streams.
 */
const cleanStreams = (userId) => {
  getUserStreams(userId).forEach((stream) => {
    if (isStreamExpired(stream)) {
      removeUserStream(getUserStreams(userId), stream.id);
    }
  });
};

/**
 * Checks a user is allowed a new stream.
 *
 * @private
 * @param {Map} userStreams The user's streams.
 */
const canStartNewStream = userId => getUserStreams(userId).size < numAllowedStreams;

/**
 * Starts a stream.  Will throw an error if the user is unable to start a new stream.
 *
 * @param {String} userId The id of the user
 * @param {String} contentId The id of the content to be streamed
 *
 * @returns {String} unique identifier for the stream
 *
 * @throws {Error} When a user cannot start a new stream
 */
const startStream = (userId, contentId) => {
  cleanStreams(userId);

  if (!canStartNewStream(userId)) {
    throw new Error('Cannot start new stream');
  }

  return addUserStream(getUserStreams(userId), contentId);
};

/**
 * Ends the stream with the given `streamId`.
 *
 * @param {String} userId The id of the user
 * @param {String} streamId The id of the stream
 */
const endStream = (userId, streamId) => {
  cleanStreams(userId);

  removeUserStream(getUserStreams(userId), streamId);
};

/**
 * Refreshes the stream with the given `streamId` to indicate it is still active.
 *
 * @param {String} userId The id of the user
 * @param {String} streamId The id of the stream
 */
const refreshStream = (userId, streamId) => {
  cleanStreams(userId);

  updateUserStream(getUserStreams(userId), streamId);
};

module.exports = {
  startStream,
  endStream,
  refreshStream,
};
