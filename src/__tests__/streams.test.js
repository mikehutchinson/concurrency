// Mocks
const MockDate = require('mockdate');

// Module under test
const streams = require('../streams');

const testUserId = 'testUserId';
const testContentId = 'testContentId';

const testTimestamp = 1514764800000; // Jan 1st 2018 00:00:00 GMT

describe('streams', () => {
  beforeEach(() => {
    streams.resetStreams();
    MockDate.set(testTimestamp);
  });

  afterEach(() => {
    MockDate.reset();
  });

  describe('getUserStreams', () => {
    it('returns a new map of streams', () => {
      const userStreams = streams.getUserStreams(testUserId);
      expect(userStreams).not.toBe(null);
      expect(userStreams).toBeInstanceOf(Map);
      expect(userStreams.size).toBe(0);
    });

    it('returns a the same map of streams for a repeated userId', () => {
      const firstUserStreams = streams.getUserStreams(testUserId);
      const secondUserStreams = streams.getUserStreams(testUserId);
      expect(firstUserStreams).toBe(secondUserStreams);
    });

    it('returns a the different map of streams for different userId', () => {
      const firstUserStreams = streams.getUserStreams(testUserId);
      const secondUserStreams = streams.getUserStreams('notTheSameUserId');
      expect(firstUserStreams).not.toBe(secondUserStreams);
    });
  });

  describe('addUserStream', () => {
    let userStreams;

    beforeEach(() => {
      userStreams = new Map();
    });

    it('returns null if no userStreams provided', () => {
      expect(streams.addUserStream(null, testContentId)).toBeNull();
    });

    it('adds a stream object to the userStreams with a generated uuid', () => {
      const streamId = streams.addUserStream(userStreams, testContentId);

      expect(streamId).not.toBe(null);
      expect(userStreams.size).toBe(1);

      const stream = userStreams.get(streamId);

      expect(stream).not.toBeNull();
      expect(stream).toEqual({
        id: streamId,
        contentId: testContentId,
        createdAt: testTimestamp,
        updatedAt: testTimestamp,
      });
    });

    it('adds a second stream object to the userStreams when reusing contentId', () => {
      const firstStreamId = streams.addUserStream(userStreams, testContentId);
      const secondStreamId = streams.addUserStream(userStreams, testContentId);

      expect(firstStreamId).not.toEqual(secondStreamId);
      expect(userStreams.size).toBe(2);

      const firstStream = userStreams.get(firstStreamId);
      const secondStream = userStreams.get(secondStreamId);
      expect(firstStream).not.toEqual(secondStream);

      expect(firstStream.contentId).toBe(testContentId);
      expect(secondStream.contentId).toBe(testContentId);
    });

    it('adds a second stream object to the userStreams when using different contentId', () => {
      const firstStreamId = streams.addUserStream(userStreams, testContentId);
      const secondStreamId = streams.addUserStream(userStreams, 'aDifferentContentId');

      expect(firstStreamId).not.toEqual(secondStreamId);
      expect(userStreams.size).toBe(2);

      const firstStream = userStreams.get(firstStreamId);
      const secondStream = userStreams.get(secondStreamId);
      expect(firstStream).not.toEqual(secondStream);

      expect(firstStream.contentId).toBe(testContentId);
      expect(secondStream.contentId).toBe('aDifferentContentId');
    });
  });

  describe('removeUserStream', () => {
    const testStreamId = 'testStreamId';
    let userStreams;

    beforeEach(() => {
      userStreams = new Map();
      const addUserStream = i => userStreams.set(`${testStreamId}${i}`, {
        id: `${testStreamId}${i}`,
        contentId: testContentId,
        createdAt: Date.now(),
        updatedAt: Date.now(),
      });

      addUserStream(1);
      addUserStream(2);
    });

    it('does nothing when userStreams is not provided', () => {
      expect(() => streams.removeUserStream(null, `${testStreamId}1`)).not.toThrow();
    });

    it('removes the stream when the streamId exists', () => {
      streams.removeUserStream(userStreams, `${testStreamId}1`);

      expect(userStreams.size).toBe(1);

      expect(userStreams.has(`${testStreamId}1`)).not.toBe(true);
      expect(userStreams.has(`${testStreamId}2`)).toBe(true);
    });

    it('does not remove any stream when the streamId does not exist', () => {
      streams.removeUserStream(userStreams, 'notAStreamId');

      expect(userStreams.size).toBe(2);

      expect(userStreams.has(`${testStreamId}1`)).toBe(true);
      expect(userStreams.has(`${testStreamId}2`)).toBe(true);
    });
  });

  describe('updateUserStream', () => {
    const testStreamId = 'testStreamId';
    let userStreams;

    beforeEach(() => {
      userStreams = new Map();
      const addUserStream = i => userStreams.set(`${testStreamId}${i}`, {
        id: `${testStreamId}${i}`,
        contentId: testContentId,
        createdAt: Date.now(),
        updatedAt: Date.now(),
      });

      addUserStream(1);
      addUserStream(2);
    });

    it('does nothing when userStreams is not provided', () => {
      expect(() => streams.updateUserStream(null, `${testStreamId}1`)).not.toThrow();
    });

    it('replaces existing stream with new updatedAt value', () => {
      const existingStream = { ...userStreams.get(`${testStreamId}1`) };

      MockDate.set(testTimestamp + 1000);
      streams.updateUserStream(userStreams, `${testStreamId}1`);

      expect(userStreams.size).toBe(2);
      expect(userStreams.get(`${testStreamId}1`)).not.toBe(existingStream);
      expect(userStreams.get(`${testStreamId}1`).updatedAt).toBe(testTimestamp + 1000);
    });

    it('does nothing if no existing stream', () => {
      streams.updateUserStream(userStreams, 'notAStream');

      expect(userStreams.size).toBe(2);
    });
  });

  describe('getUserStream', () => {
    const testStreamId = 'testStreamId';
    let userStreams;
    let testStream1;

    beforeEach(() => {
      userStreams = new Map();
      const addUserStream = i => userStreams.set(`${testStreamId}${i}`, {
        id: `${testStreamId}${i}`,
        contentId: testContentId,
        createdAt: Date.now(),
        updatedAt: Date.now(),
      });

      addUserStream(1);
      addUserStream(2);

      testStream1 = userStreams.get(`${testStreamId}1`);
    });

    it('returns a users stream when it exists', () => {
      expect(streams.getUserStream(userStreams, `${testStreamId}1`)).toEqual(testStream1);
    });

    it('returns a new instances of the same stream when called multiple times', () => {
      const firstInstance = streams.getUserStream(userStreams, `${testStreamId}1`);
      const secondInstance = streams.getUserStream(userStreams, `${testStreamId}1`);

      expect(firstInstance).not.toBe(secondInstance);
      expect(firstInstance).toEqual(secondInstance);
    });

    it('returns null when stream does not exist', () => {
      expect(streams.getUserStream(userStreams, 'notAStream')).toBeNull();
    });

    it('returns null when userStreams is invalid', () => {
      expect(streams.getUserStream(null, `${testStreamId}1`)).toBeNull();
    });
  });
});
