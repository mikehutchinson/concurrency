// Mocks
const mockApp = {
  listen: jest.fn(),
  get: jest.fn(),
  post: jest.fn(),
};
const mockExpress = jest.fn().mockImplementation(() => mockApp);
jest.mock('express', () => mockExpress);

const mockExpressSwagger = jest.fn();
const mockExpressSwaggerGenerator = jest.fn(() => mockExpressSwagger);
jest.mock('express-swagger-generator', () => mockExpressSwaggerGenerator);

// Modules
const apiDocsRedirect = require('../routes/apiDocsRedirect');
const { startStream, stopStream } = require('../routes/stream');
const { heartbeat } = require('../routes/heartbeat');
const { encodeUserId } = require('../routes/helper');

// Module under test
const app = require('../index.js');

describe('Server', () => {
  test('should setup server with swagger', () => {
    expect(mockExpressSwaggerGenerator).toBeCalledWith(app);
    expect(mockExpressSwagger).toHaveBeenCalled();
    expect(mockExpress).toHaveBeenCalled();
  });

  test('should listen on port 3000', () => {
    expect(app.listen).toBeCalledWith(3000);
  });

  test('should redirect / to /api-docs', () => {
    expect(app.get).toHaveBeenCalledWith('/', apiDocsRedirect);
  });

  test('should bind POST /user/:userId to encodeUserId', () => {
    expect(app.post).toHaveBeenCalledWith('/user/:userId', encodeUserId);
  });

  test('should bind POST /start/:contentId to startStream', () => {
    expect(app.post).toHaveBeenCalledWith('/start/:contentId', startStream);
  });

  test('should bind POST /stop/:streamId to stopStream', () => {
    expect(app.post).toHaveBeenCalledWith('/stop/:streamId', stopStream);
  });

  test('should bind POST /heartbeat/:streamId to heartbeat', () => {
    expect(app.post).toHaveBeenCalledWith('/heartbeat/:streamId', heartbeat);
  });
});
