// Mocks
const MockDate = require('mockdate');

const mockStreams = {
  getUserStreams: jest.fn(),
  addUserStream: jest.fn(),
  removeUserStream: jest.fn(),
  updateUserStream: jest.fn(),
};
jest.mock('../streams', () => mockStreams);

// Module under test
const concurrency = require('../concurrency');

const { ConcurrencyExceededError } = concurrency;
const testTimestamp = 1514764800000; // Jan 1st 2018 00:00:00 GMT

describe('concurrency', () => {
  let userStreams;
  const testUserId = 'testUserId';
  const testContentId = 'testContentId';
  const testStreamId = 'testStreamId';

  beforeEach(() => {
    userStreams = new Map();
    mockStreams.getUserStreams.mockImplementation(() => userStreams);
    mockStreams.addUserStream.mockImplementation(() => testStreamId);
    mockStreams.removeUserStream
      .mockImplementation((streams, streamId) => streams.delete(streamId));

    MockDate.set(testTimestamp);
  });

  afterEach(() => {
    mockStreams.getUserStreams.mockReset();
    mockStreams.addUserStream.mockReset();
    mockStreams.removeUserStream.mockReset();
  });

  describe('startStream', () => {
    it('adds a stream when all all ok', () => {
      const streamId = concurrency.startStream(testUserId, testContentId);

      expect(streamId).toBe(testStreamId);

      expect(mockStreams.getUserStreams).toHaveBeenCalledWith(testUserId);
      expect(mockStreams.addUserStream).toHaveBeenCalledWith(userStreams, testContentId);
    });

    it('throws when user has exceeded number of streams', () => {
      const addStream = (id, addToTimestamp = 0) => userStreams.set(id, { id, updatedAt: testTimestamp + addToTimestamp });
      addStream(1);
      addStream(2);
      addStream(3);

      MockDate.set(testTimestamp + 59999);

      expect(() => concurrency.startStream(testUserId, testContentId)).toThrow(ConcurrencyExceededError);

      expect(mockStreams.getUserStreams).toHaveBeenCalledWith(testUserId);
      expect(mockStreams.addUserStream).not.toHaveBeenCalled();
    });

    it('adds a stream when some have expired', () => {
      const addStream = (id, addToTimestamp = 0) => userStreams.set(id, { id, updatedAt: testTimestamp + addToTimestamp });

      addStream(1, 30000);
      addStream(2, 30000);
      addStream(3); // Will be the expired stream

      MockDate.set(testTimestamp + 60000);

      const streamId = concurrency.startStream(testUserId, testContentId);
      expect(streamId).toBe(testStreamId);

      expect(mockStreams.getUserStreams).toHaveBeenCalledWith(testUserId);
      expect(mockStreams.addUserStream).toHaveBeenCalledWith(userStreams, testContentId);
      expect(mockStreams.removeUserStream).toHaveBeenCalledWith(userStreams, 3);
    });
  });

  describe('endStream', () => {
    it('ends a stream when all all ok', () => {
      concurrency.endStream(testUserId, testStreamId);

      expect(mockStreams.getUserStreams).toHaveBeenCalledWith(testUserId);
      expect(mockStreams.removeUserStream).toHaveBeenCalledWith(userStreams, testStreamId);
    });
  });

  describe('refreshStream', () => {
    it('refreshes a stream when all all ok', () => {
      concurrency.refreshStream(testUserId, testStreamId);

      expect(mockStreams.getUserStreams).toHaveBeenCalledWith(testUserId);
      expect(mockStreams.updateUserStream).toHaveBeenCalledWith(userStreams, testStreamId);
    });
  });
});
