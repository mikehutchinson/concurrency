const express = require('express');

const app = express();
const expressSwagger = require('express-swagger-generator')(app);
const { version } = require('../package.json');
const apiDocs = require('./routes/apiDocsRedirect');
const { startStream, stopStream } = require('./routes/stream');
const { heartbeat } = require('./routes/heartbeat');
const { encodeUserId } = require('./routes/helper');

const options = {
  swaggerDefinition: {
    info: {
      description: 'A extremely simple concurrency service.\n\
\n\
Ensure you first retrieve a token using the `/user/{userId}` endpoint.\n\
Then add the token value as the Authorization header in Bearer form `Bearer <Token>`.\n\
\n\
I am sure there is a way to get Swagger to automatically set the Auth header from this endpoint,\
 but I have not had time to figure it out.',
      title: 'Concurrency',
      version,
    },
    host: 'localhost:3000',
    basePath: '',
    produces: [
      'application/json',
    ],
    schemes: ['http'],
    securityDefinitions: {
      JWT: {
        type: 'apiKey',
        in: 'header',
        name: 'Authorization',
        description: 'JWT Bearer Token ("Bearer <Token>"). Get a token from the POST /user/:userId endpoint.',
      },
    },
  },
  basedir: __dirname,
  files: ['./routes/**/*.js'],
};
expressSwagger(options);

// Redirect root to swagger docs.
app.get('/', apiDocs);
app.post('/user/:userId', encodeUserId);

app.post('/start/:contentId', startStream);
app.post('/stop/:streamId', stopStream);

app.post('/heartbeat/:streamId', heartbeat);

app.listen(3000);

module.exports = app;
